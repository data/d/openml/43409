# OpenML dataset: Historical-Weather-data-of-Goa-India

https://www.openml.org/d/43409

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
I am currently building a short term (one-day ahead) electric load forecasting model for Goa. A good chunk of it is domestic household load. Temperature and Humidity can be used to estimate the heat index which can be used as one of the input variables for the model. Hence, future temperature and humidity predictions are essential.
Content
Contains following data from 01 July'16 - 31 July'19 with 15 minute resolution 
Temperature (K);Temperature at 2 m above ground    
 Relative humidity ();Relative humidity at 2 m above ground    
 Pressure (hPa);Pressure at ground level    
 Wind speed (m/s);Wind speed at 10 m above ground    
 Wind direction (deg);Wind direction at 10 m above ground (0 means from North     90 from East)
 Rainfall (kg/m2);Rainfall mass density per m2 area
 Short-wave irradiation (Wh/m2);Surface incoming shortwave irradiation (broadband)    
Acknowledgements
Data downloaded from:
http://www.soda-pro.com/web-services/meteo-data/merra
Provider:
National Aeronautics and Space Administration (NASA) / Goddard Space Flight Center    
More information at;
http://gmao.gsfc.nasa.gov/reanalysis/MERRA-2    
Licenses:
http://www.soda-pro.com/documents/10157/326300/CAMS-data-license.pdf
http://www.soda-pro.com/documents/10157/326300/SoDa-license.pdf
Inspiration
Future prediction of humidity and temperature using the available past data.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43409) of an [OpenML dataset](https://www.openml.org/d/43409). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43409/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43409/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43409/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

